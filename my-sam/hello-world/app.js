// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */
'use strict'

const aws=require('aws-sdk')
const bcrypt=require('bcryptjs')
const dynamodb=new aws.DynamoDB.DocumentClient();
const s3=new aws.s3()


exports.lambdaHandler = async (event, context) => {
    try {
        // const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'hello world',
                // location: ret.data.trim()
            })
        }
    } catch (err) {
        console.log(err);
        return err;
    }

    return response
};

exports.UserCreate=(event,context,callback)=>{
    const requestbody=JSON.parse(event.body)

    createUsers(requestbody)
    .then(res=>{
        if(res){
            callback(null,
                {status:200,
                 body:JSON.stringify({message:res+"this responce"})})
        }
    })
    .catch(err=>{
        if(err){
            callback(null,{statusCode:404,body:JSON.stringify({
                message:"unable to submit"
            })})
        }
    })


    

}


function createUsers(body){
    return new Promise((resolve,reject)=>{
        findUserByEmail(body.email)
        .then(res=>{
            if(!res){
                return reject(404)
            }else{
                insertUser(body)
            }
        })
        .then(resolve(true))
        .catch(err=>{
            reject(err)
        })

    })
}


function findUserByEmail(email){
    console.log("find user by email")
    return new Promise((resolve,reject)=>{
        dynamodb.query({
            TableName:'user',
            KeyConditionExpression:'#email=:email',
            ExpressionAttributeNames:{
                '#email':'email'
            },ExpressionAttributeValues:{
                ':email':email
            }
        },(err,data)=>{
            console.log("err"+err+"data:"+data)
         if(data.Items[0]){
                resolve(false)
         }else{
                resolve(true)
        }if(err){
            reject(err)
            

        }
           
        })

    })
}


function insertUser(body){
    console.log("this is in insert user")
    return new Promise((resolve,reject)=>{
        bcrypt.genSalt(10,(err,salt)=>{
            bcrypt.hash(body.password,salt,(err,hash)=>{
                body.password=hash;
                console.log(hash)
                dynamodb.put({
                    TableName:'user',
                    Item:{
                        email:body.email,
                        User_id:Date.now(),
                        First_name:body.First_name,
                        password:body.password
                    }
                },(err,data)=>{
                    if(err){
                        reject(err)
                    }else{
                        resolve(data)
                    }
                })
            })
        })
  })
}


exports.uploadImage=(event,context,callback)=>{
    let encodedimage=JSON.parse(event.body)
    let decodeImage=Buffer.from(encodedimage,'base64')
    var filePath="avatars/"+event.queryStringParameters.username+".jpg"
    var param={
        "Body":decodeImage,
        "Bucket":"newsam",
        "Key":filePath
    };
    s3.upload(param,(err,data)=>{
        if(err){
            callback(err,null)
        }else{
            let responce={
                "statusCode":200,
                "headers":{
                    "my_header":"my_value"
                },
                "body":JSON.stringify(data),
                "isBase64Encoded":false
            }
            callback(null,responce)
        }
    })




}



exports.login=(event,context,callback)=>{

}

function emailCheck(){

}

function verifyPass(){

}


function goInsertToken(){

}

function tokenGenaration(){
    
}



